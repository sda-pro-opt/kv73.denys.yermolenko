(uiop:define-package :transl/parser
    (:use :cl
          :transl/lexem
          :transl/deque
          :transl/lexer)
  (:import-from :alexandria
                :switch)
  (:export :parser))

(in-package :transl/parser)


(defun parser (lexer-output)
  (let ((lexems (first lexer-output))
        (node-childs-stack (list (cons nil nil)))
        (node-type-stack (list 'signal-program))
        (op-address-stack)
        (ret-value nil)
        (node-info-list-stack)
        (error-message)
        (result))
    (labels ((%cur-lex-id= (lexem-id)
               (when lexems
                 (eq lexem-id (lexem-id (lexem (first lexems))))))
             (%cur-lex-type= (lexem-type)
               (when lexems
                 (eq lexem-type (lexem-type (lexem (first lexems))))))
             (%gen-ast-part (&key node-info)
               (let* ((current-childs (pop node-childs-stack))
                      (first-child (car current-childs))
                      (second-child (cdr current-childs)))
                 (%add-to-current-childs-list 
                  (list (first node-type-stack)
                        node-info
                        first-child
                        second-child))))
             (%gen-final-result ()
               (list (list (pop node-type-stack)
                           '(first-child)
                           (first (pop node-childs-stack))
                           nil)
                     error-message))
             (%gen-error-message (expected-terminal)
               (if lexems
                   (let ((current-lexem (first lexems)))
                     (setf error-message
                           (format nil
                                   "Parser: Error (line ~A , column ~A): '~A' expected but '~A' found"
                                   (line-pos current-lexem)
                                   (column-pos current-lexem)
                                   expected-terminal
                                   (lexem-string (lexem current-lexem)))))
                   (setf error-message
                         (format nil
                                 "Parser: Error : '~A' expected but end of file found"
                                 expected-terminal))))
             (%add-to-current-childs-list (node)
               (let ((current-node-childs (first node-childs-stack)))
                 (if (first current-node-childs)
                     (rplacd current-node-childs node)
                     (rplaca current-node-childs node))))
             (%gen-terminal-node (node-type node-info)
               (%add-to-current-childs-list (list node-type node-info)))
             (%add-to-node-type-stack (node-type)
               (push node-type node-type-stack))
             (%add-new-list-to-node-childs-stack ()
               (push (cons nil nil) node-childs-stack))
             (%add-to-current-node-info-list (node-info)
               (push-end (first node-info-list-stack)
                         node-info))
             (%add-list-to-node-info-stack-list ()
               (push (make-deque) node-info-list-stack))
             (%add-to-op-address-stack (op-address)
               (push op-address op-address-stack))
             (%set-current-op-address (op-address)
               (setf (first op-address-stack) op-address))
             (%ret-value (value &key inside-cur-node)
               (setf ret-value value)
               (if inside-cur-node
                   (%set-current-op-address 0)
                   (progn (pop op-address-stack)
                          (pop node-type-stack))))
             (%set-to-next-lexem ()
               (setf lexems (rest lexems)))
             (%to-next-address-when-lexem-id= (id &key expected next-op cur-node-can-be-empty ret-val)
               (if (%cur-lex-id= id)
                   (progn
                     (%add-to-current-node-info-list (first lexems))
                     (%set-to-next-lexem)
                     (setf ret-value (when ret-val
                                       ret-val))
                     (if next-op
                         (setf (first op-address-stack) next-op)
                         (incf (first op-address-stack))))
                   (if cur-node-can-be-empty
                       (progn
                         (%add-to-current-node-info-list 'first-child)
                         (%gen-terminal-node 'empty nil)
                         (%ret-value 't :inside-cur-node t))
                       (progn
                         (%gen-error-message expected)
                         (%ret-value 'f :inside-cur-node t)))))
             (%go-to-node (node-type)
               (%add-to-node-type-stack node-type)
               (%add-new-list-to-node-childs-stack)
               (%add-list-to-node-info-stack-list)
               (setf ret-value nil)
               (%add-to-op-address-stack 0))
             (%end-curr-node-check (&key ret-value)
               (%gen-ast-part
                :node-info (deque-buffer (pop node-info-list-stack)))
               (%ret-value ret-value)))
      (loop
         :while node-type-stack
         :do
           (switch ((first node-type-stack))
             ('signal-program           ;(break)
              (switch (ret-value)
                (nil
                                        ;(break)
                 (%go-to-node 'program))
                ('t (setf result (%gen-final-result))
                                        ;(return)
                    )
                ('f (setf result (%gen-final-result))
                                        ;(return)
                    )))
             ('program
              (switch ((first op-address-stack))
                (0                      ;(break)
                 (switch (ret-value)
                   (nil (%to-next-address-when-lexem-id= 400
                                                         :expected "PROGRAM"))
                   ('t (%end-curr-node-check :ret-value 't))
                   ('f (%end-curr-node-check :ret-value 'f))))
                (1                      ;(break)
                 (switch (ret-value)
                   (nil (%go-to-node 'procedure-identifier))
                   ('t
                    (%add-to-current-node-info-list 'first-child)
                    (%set-current-op-address 2))
                   ('f
                    (%add-to-current-node-info-list 'first-child)
                    (%ret-value 'f
                                :inside-cur-node t))))
                (2
                 (%to-next-address-when-lexem-id= 500
                                                  :expected ";"))
                (3
                 (switch (ret-value)
                   (nil (%go-to-node 'block))
                   ('t
                    (%add-to-current-node-info-list 'second-child)
                    (%set-current-op-address 4))
                   ('f
                    (%add-to-current-node-info-list 'second-child)
                    (%ret-value 'f :inside-cur-node t))))
                (4
                                        ;(break)
                 (%to-next-address-when-lexem-id= 502
                                                  :expected "."
                                                  :next-op 0
                                                  :ret-val 't))))
             ('block
                 (switch ((first op-address-stack))
                   (0                   ;(break)
                    (switch (ret-value)
                      (nil (%set-current-op-address 1))
                      ('t (%end-curr-node-check :ret-value 't))
                      ('f (%end-curr-node-check :ret-value 'f))))
                   (1
                    (switch (ret-value)
                      (nil (%go-to-node 'declarations))
                      ('t
                       (%add-to-current-node-info-list 'first-child)
                       (%set-current-op-address 2))
                      ('f
                       (%add-to-current-node-info-list 'first-child)
                       (%ret-value 'f :inside-cur-node t))))
                   (2
                                        ;(break)
                    (%to-next-address-when-lexem-id= 401
                                                     :expected "BEGIN"))
                   (3                   ;(break)
                    (switch (ret-value)
                      (nil (%go-to-node 'statements-list))
                      ('t
                       (%add-to-current-node-info-list 'second-child)
                       (%set-current-op-address 4))
                      ('f
                       (%add-to-current-node-info-list 'second-child)
                       (%ret-value 'f :inside-cur-node t))))
                   (4
                    (%to-next-address-when-lexem-id= 402
                                                     :expected "END"
                                                     :next-op 0
                                                     :ret-val 't))))
             ('declarations             ;(break)
              (switch (ret-value)
                (nil (%go-to-node 'constant-declarations))
                ('t
                 (%add-to-current-node-info-list 'first-child)
                 (%end-curr-node-check :ret-value 't))
                ('f
                 (%add-to-current-node-info-list 'first-child)
                 (%end-curr-node-check :ret-value 'f))))
             ('constant-declarations
              (switch ((first op-address-stack))
                (0                      ;(break)
                 (switch (ret-value)
                   (nil (%to-next-address-when-lexem-id= 403
                                                         :expected "CONST"
                                                         :cur-node-can-be-empty t))
                   ('t (%end-curr-node-check :ret-value 't))
                   ('f (%end-curr-node-check :ret-value 'f))))
                (1
                 (switch (ret-value)
                   (nil (%go-to-node 'constant-declarations-list))
                   ('t (%add-to-current-node-info-list 'first-child)
                       (%ret-value 't :inside-cur-node t))
                   ('f
                    (%add-to-current-node-info-list 'first-child)
                    (%ret-value 'f :inside-cur-node t)))))) 
             ('constant-declarations-list
              (switch ((first op-address-stack))
                (0
                 (switch (ret-value)
                   (nil (if (%cur-lex-type= 'transl/lexer:ident)
                            (%set-current-op-address 1)
                            (progn
                              (%add-to-current-node-info-list 'first-child)
                              (%gen-terminal-node 'empty nil)
                              (%end-curr-node-check :ret-value 't))))
                   ('t (%end-curr-node-check :ret-value 't))
                   ('f (%end-curr-node-check :ret-value 'f))))
                (1
                 (switch (ret-value)
                   (nil
                    (%go-to-node 'constant-declaration))
                   ('t (%add-to-current-node-info-list 'first-child)
                       (setf ret-value nil)
                       (%set-current-op-address 2))
                   ('f
                    (%add-to-current-node-info-list 'first-child)
                    (%ret-value 'f :inside-cur-node t))))
                (2
                 (switch (ret-value)
                   (nil (%go-to-node 'constant-declarations-list))
                   ('t
                    (%add-to-current-node-info-list 'second-child)
                    (%ret-value 't :inside-cur-node t))
                   ('f
                    (%add-to-current-node-info-list 'second-child)
                    (%ret-value 'f :inside-cur-node t))))))
             ('constant-declaration
              (switch ((first op-address-stack))
                (0
                 (switch (ret-value)
                   (nil (%set-current-op-address 1))
                   ('t (%end-curr-node-check :ret-value 't))
                   ('f
                    (%end-curr-node-check :ret-value 'f))))
                (1 
                 (switch (ret-value)
                   (nil (%go-to-node 'constant-identifier))
                   ('t
                    (%add-to-current-node-info-list 'first-child)
                    (%set-current-op-address 2))
                   ('f
                    (%add-to-current-node-info-list 'first-child)
                    (%ret-value 'f :inside-cur-node t))))
                (2
                 (%to-next-address-when-lexem-id= 501
                                                  :expected "="))
                (3
                 (if (or (%cur-lex-type= 'transl/lexer:int)
                         (%cur-lex-type= 'transl/lexer:float))
                     (progn (%gen-terminal-node 'constant (first lexems))
                            (%set-to-next-lexem)
                            (%add-to-current-node-info-list 'second-child)
                            (%set-current-op-address 4))
                     (progn
                       (%gen-error-message "<constant>")
                       (%ret-value 'f :inside-cur-node t))))
                (4
                 (%to-next-address-when-lexem-id= 500
                                                  :expected ";"
                                                  :next-op 0
                                                  :ret-val 't))))
             ('constant-identifier
              (if (%cur-lex-type= 'ident)
                  (progn
                    (%gen-terminal-node 'identifier (first lexems))
                    (%set-to-next-lexem)
                    (%add-to-current-node-info-list 'first-child)
                    (%end-curr-node-check :ret-value 't))
                  (progn
                    (%gen-error-message "<identifier>")
                    (%end-curr-node-check :ret-value 'f))))
             ('procedure-identifier
                                        ;(break)
              (if (%cur-lex-type= 'ident)
                  (progn
                    (%gen-terminal-node 'identifier (first lexems))
                    (%set-to-next-lexem)
                    (%add-to-current-node-info-list 'first-child)
                    (%end-curr-node-check :ret-value 't))
                  (progn
                    (%gen-error-message "<identifier>")
                    (%end-curr-node-check :ret-value 'f))))
             ('statements-list
                                        ;(break)
              (%add-to-current-node-info-list 'first-child)
              (%gen-terminal-node 'empty nil)
              (%end-curr-node-check :ret-value 't)))))
    (when (and lexems (not error-message))
      (let ((lexem-container (first lexems)))
        (setf (second result)
              (format nil 
                      "Parser: Error (line ~A, column ~A): Unexpected '~A' after program end~%"
                      (line-pos lexem-container)
                      (column-pos lexem-container)
                      (lexem-string (lexem lexem-container))))))
    result))
