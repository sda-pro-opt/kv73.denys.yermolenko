(uiop:define-package :transl/deque
    (:use :cl)
  (:export #:make-deque
           :deque-buffer
           #:push-start
           #:push-end
           #:pop-start))

(in-package :transl/deque)

(defclass deque ()
    ((head-pointer
      :accessor head-pointer
      :initform nil)
     (buffer
      :accessor deque-buffer
      :initform '())))

(defmethod make-deque ()
  (make-instance 'deque))

(defgeneric push-start (deque el)
  (:documentation "push el to the start of deque"))

(defgeneric pop-start (deque)
  (:documentation "pop el form the start of deque")) 

(defgeneric push-end (deque el)
  (:documentation "push el to the end of deque"))

(defmethod push-start ((deque deque) el)
  (with-slots (head-pointer buffer) deque
    (push el buffer)
    (unless head-pointer
      (setf head-pointer buffer))
    buffer))

(defmethod pop-start ((deque deque))
  (with-slots (head-pointer buffer) deque
    (pop buffer)))

(defmethod push-end ((deque deque) el)
  (with-slots (head-pointer buffer) deque
    (if head-pointer
        (progn
          (rplacd head-pointer (list el))
          (setf head-pointer (rest head-pointer)))
        (progn
          (push el buffer)
          (setf head-pointer buffer)))
    buffer))

(defmethod print-object ((deque deque) stream)
  (with-slots (buffer) deque
      (print-object buffer stream)))
