(asdf:defsystem "transl"
    :class :package-inferred-system
    :defsystem-depends-on (:asdf-package-system)
    :description "Partial SIGNAL language translator"
    :version "0.0.1"
    :author "Ermolenko Denis"
    :depends-on ("transl/all" "alexandria" "lisp-unit")
    :perform (test-op (o c) (uiop:symbol-call :lisp-unit :run-tests :all :transl/tests)))
