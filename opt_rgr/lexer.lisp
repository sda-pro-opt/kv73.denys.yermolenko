(uiop:define-package :transl/lexer
    (:use :cl :transl/lexem)
  (:import-from :alexandria
                :switch
                :curry)
  (:export lexer
           ident
           float
           int))

(in-package :transl/lexer)

(defvar predefine-table
  (let ((hash-table (make-hash-table :test 'equal)))
    (labels ((%add-to-table (string id type)
               (setf (gethash string hash-table)
                     (make-lexem-inst :type type :id id :string string))))
      (%add-to-table "PROGRAM" 400 'keyword)
      (%add-to-table "BEGIN" 401 'keyword)
      (%add-to-table "END" 402 'keyword)
      (%add-to-table "CONST" 403 'keyword)
      (%add-to-table ";" 500 'delimeter)
      (%add-to-table "=" 501 'delimeter)
      (%add-to-table "." 502 'delimeter)
      hash-table)))

(defparameter ascii-table
  (let ((ascii-hash-table (make-hash-table)))
    (labels ((%add-to-table (char-code type)
               (setf (gethash (code-char char-code) ascii-hash-table) type)))
      (loop for i from 48 to 57
         do (%add-to-table i 'digit))
      (loop for i from 65 to 90
         do (%add-to-table i 'letter))
      (loop for i from 97 to 122
         do (%add-to-table i 'letter))
      (loop for i from 8 to 13
         do (%add-to-table i 'whitespace))
      (%add-to-table 32 'whitespace)
      (%add-to-table 40 'open-parenth)
      (%add-to-table 41 'close-parenth)
      (%add-to-table 42 'asterisk)
      (%add-to-table 35 'hash)
      (%add-to-table 43 'sign)
      (%add-to-table 45 'sign)
      (%add-to-table 36 'sign)
      (%add-to-table 46 'delim)
      (%add-to-table 59 'delim)
      (%add-to-table 61 'delim)
      (setf (gethash 'eof ascii-hash-table) 'eof))
    ascii-hash-table))

(defun lexer (file-name)
  (let ((ident-table (make-hash-table :test 'equal))
        (constant-table (make-hash-table :test 'equal))
        (current-state 'start)
        (current-lexem-type)
        (current-lexem-col)
        (lexem-string)
        (line-cnt 1)
        (pos-cnt 0)
        (lexems)
        (ident-id 999)
        (constant-id 1999)
        (error?)
        (end?))
    (labels ((%automat (cur-char)
               (incf pos-cnt)
               (let ((char-type (gethash cur-char ascii-table)))
                 
                 (labels ((%char-type= (type)
                            (eq char-type type)))
                   (unless (or (eq current-state 'ws)
                               (eq current-state 'com))
                     (push cur-char lexem-string))
                   (switch (current-state :test #'eq)
                     ('start (%main-selector char-type cur-char))
                     ('idn
                      (unless (or (%char-type= 'letter)
                                  (%char-type= 'digit))
                        (progn (%out-state)
                               (%main-selector char-type cur-char))))
                     ('num
                      (cond ((%char-type= 'digit) nil)
                            ((%char-type= 'hash)
                             (%set-cur-state 'num-delim))
                            (t (%out-state)
                               (%main-selector char-type cur-char))))
                     ('num-delim
                      (setf current-lexem-type 'float)
                      (cond ((%char-type= 'digit)
                             (%set-cur-state 'fract))
                            ((%char-type= 'sign)
                             (%set-cur-state 'fract-sign))
                            (t(%gen-lexer-error
                               (format nil "Incorrect float format")
                               :use-cur-lexem-pos t)
                              (%main-selector char-type cur-char))))
                     ('fract
                      (unless (%char-type= 'digit)
                        (progn (%out-state)
                               (%main-selector char-type cur-char))))
                     ('ws
                      (%new-line-check cur-char)
                      (unless (%char-type= 'whitespace)
                        (%main-selector char-type cur-char)))
                     ('bcom
                      (if (%char-type= 'asterisk)
                          (%set-cur-state 'com)
                          (progn (%gen-lexer-error
                                  (format nil "Incorrect symbol '~A' after comment start" cur-char))
                                 (%main-selector char-type cur-char))))
                     ('money-sign
                      (if (%char-type= 'digit)
                          (%set-cur-state 'money)
                          (progn (%gen-lexer-error
                                  "Incorrect money format"
                                  :use-cur-lexem-pos t)
                                 (%main-selector char-type cur-char))))
                     ('money
                      (cond ((%char-type= 'digit) nil)
                            (t (%out-state)
                               (%main-selector char-type cur-char))))
                     ('number-sign
                      (if (%char-type= 'digit)
                          (%set-cur-state 'num)
                          (progn (%gen-lexer-error
                                  "Incorrect number format"
                                  :use-cur-lexem-pos t)
                                 (%main-selector char-type cur-char))))
                     ('fract-sign
                      (if (%char-type= 'digit)
                          (%set-cur-state 'fract)
                          (progn (%gen-lexer-error
                                  "Incorrect float number format"
                                  :use-cur-lexem-pos t)
                                 (%main-selector char-type cur-char))))
                     ('com
                      (%new-line-check cur-char)
                      (cond ((%char-type= 'asterisk)
                             (%set-cur-state 'ecom))
                            ((eq cur-char 'eof)
                             (%gen-lexer-error
                              (format nil "Unexpected eof inside comment"))
                             (%main-selector char-type cur-char))
                            (t nil)))
                     ('ecom
                      (cond ((%char-type= 'asterisk) nil)
                            ((%char-type= 'close-parenth)
                             (%set-cur-state 'inp))
                            ((eq cur-char 'eof)
                             (%gen-lexer-error
                              (format nil "Unexpected eof inside comment"))
                             (%main-selector char-type cur-char))
                            (t (%set-cur-state 'com))))
                     ('inp
                      (%main-selector char-type cur-char))))))
             (%gen-lexer-error (str &key use-cur-lexem-pos)
               (setf error? t)
               (push (format nil
                             "Lexer: Error (line ~A , column ~A): ~A"
                             line-cnt
                             (if use-cur-lexem-pos
                                 current-lexem-col
                                 pos-cnt)
                             str)
                     lexems))
             (%new-line-check (cur-char)
               (when (eql cur-char  #\Newline)
                 (incf line-cnt)
                 (setf pos-cnt 0)))
             (%set-cur-state (state)
               (setf current-state state))
             (%main-selector (char-type cur-char)
               (setf lexem-string nil)
               (switch (char-type :test #'eq) 
                 ('digit
                  (setf current-lexem-type 'int)
                  (setf current-lexem-col pos-cnt)
                  (%set-cur-state 'num)
                  (push cur-char lexem-string))
                 ('letter
                  (setf current-lexem-type 'ident)
                  (setf current-lexem-col pos-cnt)
                  (%set-cur-state 'idn)
                  (push cur-char lexem-string))
                 ('sign
                  (cond ((char= cur-char #\$)  
                         (setf current-lexem-type 'money)
                         (setf current-lexem-col pos-cnt)
                         (%set-cur-state 'money-sign)
                         (push cur-char lexem-string))
                        (t (setf current-lexem-type 'int)
                           (setf current-lexem-col pos-cnt)
                           (%set-cur-state 'number-sign)
                           (push cur-char lexem-string))))
                 ('delim
                  (setf current-lexem-type 'delimeter)
                  (setf current-lexem-col pos-cnt)
                  (push cur-char lexem-string)
                  (%out-state :last-symb-not-out nil)
                  (%set-cur-state 'inp))
                 ('open-parenth
                  (%set-cur-state 'bcom))
                 ('whitespace
                  (%set-cur-state 'ws))
                 ('eof
                  (setf end? t))
                 (t (%gen-lexer-error
                     (format nil "Illegal symbol '~A' detected" cur-char))
                    (%set-cur-state 'inp))))
             (%out-state (&key (last-symb-not-out t))
                                        ;(break)
               (let* ((lexem-str (string-upcase
                                  (intern
                                   (coerce
                                    (reverse (if last-symb-not-out
                                                 (progn (pop lexem-string)
                                                        lexem-string)
                                                 lexem-string))
                                    'string))))
                      (lexem
                       (cond ((eq current-lexem-type 'ident)
                              (or (gethash lexem-str predefine-table)
                                  (gethash lexem-str ident-table)
                                  (setf (gethash lexem-str ident-table)
                                        (make-lexem-inst :string lexem-str
                                                         :id (incf ident-id)
                                                         :type 'ident))))
                             ((or (eq current-lexem-type 'int)
                                  (eq current-lexem-type 'float)
                                  (eq current-lexem-type 'money))
                              (or (gethash lexem-str constant-table)
                                  (setf (gethash lexem-str constant-table)
                                        (make-lexem-inst :string lexem-str
                                                         :id (incf constant-id)
                                                         :type current-lexem-type))))
                             ((eq current-lexem-type 'delimeter)
                              (gethash lexem-str predefine-table)))))
                 (push (make-lexem-container :lexem lexem
                                             :line-pos line-cnt
                                             :column-pos current-lexem-col)
                       lexems))))
      (with-open-file (stream file-name)
        (do ((curr-char (read-char stream nil 'eof) (read-char stream nil 'eof)))
            ((eq end? t) (list (reverse lexems) error? ident-table constant-table))
          (%automat curr-char))))))
