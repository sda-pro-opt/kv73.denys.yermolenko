(uiop:define-package :transl/frontend
    (:use :cl
          :transl/lexem)
  (:import-from :alexandria
                :maphash-values)
  (:export :print-lexer-output
           :print-parser-output))

(in-package :transl/frontend)

(defun print-lexer-output (lexer-output &key (stream t))
  (let ((lexems (first lexer-output))
        (ident-table (third lexer-output))
        (constant-table (fourth lexer-output)))
    (mapc (lambda (obj)
            (if (typep obj 'lexem-container)
                (print-lexem-container obj :stream stream)
                (format stream "~A~%" obj)))
          lexems)
    (print-ident-table ident-table stream)
    (print-constant-table constant-table stream)
    nil))


(defun print-ident-table (ident-table stream)
  (format stream "~%IDENTIFICATIONS TABLE:~%")
  (maphash-values (lambda (lexem)
                    (format stream
                            "~5A : ~A~%"
                            (lexem-id lexem)
                            (lexem-string lexem)))
                  ident-table))

(defun print-constant-table (constant-table stream)
  (format stream "~%CONSTANTS TABLE:~%")
  (maphash-values (lambda (lexem)
                    (format stream
                            "~5A : ~A ~A~%"
                            (lexem-id lexem)
                            (lexem-string lexem)
                            (lexem-type lexem)))
                  constant-table))


(defun print-parser-output (parser-output &key (stream t))
  (let ((AST-tree (first parser-output))
        (error-message (second parser-output)))
    (labels ((%print-AST (AST-tree level)
               (let ((node-type (symbol-name (first AST-tree)))
                     (node-info-list (second AST-tree))
                     (first-child (third AST-tree))
                     (second-child (fourth AST-tree))
                     (inner-level (+ level 2)))
                 (format stream "~vA<~A>~%" level #\Space node-type)
                 (if (listp node-info-list)
                     (mapc (lambda (node-info)
                             (cond ((typep node-info 'lexem-container)
                                    (%print-lexem-container node-info inner-level))
                                   ((eq node-info 'transl/parser::first-child)
                                    (%print-AST first-child inner-level))
                                   ((eq node-info 'transl/parser::second-child)
                                    (%print-AST second-child inner-level))))
                           node-info-list)
                     (%print-lexem-container node-info-list inner-level))))
             (%print-lexem-container (lexem-container level)
               (let ((lex (lexem lexem-container)))
                 (format stream
                         "~vA~A ~A~%"
                         level
                         #\Space
                         (lexem-id lex)
                         (lexem-string lex)))))
      (%print-AST AST-tree 0)
      (when error-message
        (format stream "~A" error-message)))))
