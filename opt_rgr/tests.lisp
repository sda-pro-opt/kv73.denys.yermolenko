(uiop:define-package :transl/tests
    (:use :cl
          :transl/lexer
          :transl/parser
          :transl/frontend
          :lisp-unit))

(in-package :transl/tests)

(defun compare-file (file1 file2)
  (with-open-file (f1 (asdf:system-relative-pathname 'transl file1))
    (with-open-file (f2 (asdf:system-relative-pathname 'transl file2))
      (when (and (= (file-length f1)
                    (file-length f2))
                 (loop :for char1 = (read-char f1 nil 'eof)
                       :for char2 = (read-char f2 nil 'eof)
                       :until (or (eq char1 'eof)
                                  (eq char2 'eof))
                       :always (char= char1 char2)))
        t))))

(defmacro def-test (name test-folder test-func)
  (let ((output-filename (asdf:system-relative-pathname 'transl
                                                        (concatenate 'string test-folder "/output.txt")))
        (input-filename (asdf:system-relative-pathname 'transl
                                                       (concatenate 'string test-folder "/input.sig")))
        (expected-filename (asdf:system-relative-pathname 'transl
                                                          (concatenate 'string test-folder "/expected.txt"))))
    `(define-test ,name
       (let ((input-filename ,input-filename))
         (with-open-file (outfile ,output-filename :direction :output :if-exists :supersede)
           ,test-func))
       (assert-true (compare-file ,output-filename ,expected-filename)))))

(defmacro def-lexer-test (name test-folder)
  `(def-test ,name ,(concatenate 'string "tests/lexer/" test-folder)
     (print-lexer-output (lexer input-filename)
                         :stream outfile)))

(defmacro def-parser-test (name test-folder)
  `(def-test ,name ,(concatenate 'string "tests/parser/" test-folder)
     (print-parser-output (parser (lexer input-filename))
                          :stream outfile)))


(def-lexer-test test1
    "test-1")

(def-lexer-test test2
    "test-2")

(def-lexer-test test3
    "test-3")

(def-lexer-test test4
    "test-4")

(def-lexer-test test5
    "test-5")

(def-lexer-test fail-test1
    "fail-test-1")

(def-lexer-test fail-test2
    "fail-test-2")

(def-lexer-test fail-test3
    "fail-test-3")

(def-parser-test pars-test-1 "test-1")

(def-parser-test pars-test-2 "test-2")

(def-parser-test pars-test-3 "test-3")

(def-parser-test pars-test-4 "test-4")

(def-parser-test pars-test-fail-1 "fail-test-1")

(def-parser-test pars-test-fail-2 "fail-test-2")

(def-parser-test pars-test-fail-3 "fail-test-3")

(def-parser-test pars-test-fail-4 "fail-test-4")

(def-parser-test pars-test-fail-5 "fail-test-5")

(def-parser-test pars-test-fail-6 "fail-test-6")

(def-parser-test pars-test-fail-7 "fail-test-7")

(def-parser-test pars-test-fail-8 "fail-test-8")

(def-parser-test pars-test-fail-9 "fail-test-9")

(def-parser-test pars-test-fail-10 "fail-test-10")

(def-parser-test pars-test-fail-11 "fail-test-11")

(def-parser-test pars-test-fail-12 "fail-test-12")
