(uiop:define-package :transl/lexem
    (:export #:make-lexem-inst
             #:make-lexem-container
             #:print-lexem-container
             :lexem-container
             :lexem
             :lexem-string
             :lexem-id
             :lexem-type
             :line-pos
             :column-pos))

(in-package :transl/lexem)

(defclass lexem ()
  ((string
    :accessor lexem-string
    :initarg :string)
   (id
    :accessor lexem-id
    :initarg :id)
   (type
    :accessor lexem-type
    :initarg :type)))

(defclass lexem-container ()
  ((line-pos
    :accessor line-pos
    :initarg :line-pos)
   (column-pos
    :accessor column-pos
    :initarg :column-pos)
   (lexem
    :accessor lexem
    :initarg :lexem)))

(defun make-lexem-inst (&key string id type)
  (make-instance 'lexem :string string :id id :type type))

(defun make-lexem-container (&key lexem line-pos column-pos)
  (make-instance 'lexem-container :lexem lexem :line-pos line-pos :column-pos column-pos))

(defun print-lexem-container (lex-cont &key (stream t))
  (let* ((line-pos (line-pos lex-cont))
         (column-pos (column-pos lex-cont))
         (lexem (lexem lex-cont))
         (id (lexem-id lexem))
         (str (lexem-string lexem)))
    (format stream "~3A ~3A ~5A ~A~%" line-pos column-pos id str)))
