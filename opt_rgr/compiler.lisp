(uiop:define-package :transl/compiler
    (:use :cl
          :transl/lexer
          :transl/parser
          :transl/frontend)
  (:export #:syntax-analyzer))

(in-package :transl/compiler)

(defun syntax-analyzer (input-filename &key with-lexer-out (stream t))
  (with-open-file (input-file input-filename)
    (let ((lexer-output (lexer input-file)))
      (when (or (second lexer-output)
                with-lexer-out)
        (print-lexer-output lexer-output :stream stream))
      (unless (second lexer-output)
        (print-parser-output (parser lexer-output) :stream stream)))))
