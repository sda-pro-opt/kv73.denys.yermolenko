(uiop:define-package all
    (:use :transl/deque
          :transl/lexer
          :transl/lexem
          :transl/parser
          :transl/frontend
          :transl/tests
          :transl/compiler))
